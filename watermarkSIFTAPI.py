#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import cv2
import math
import os
import sys
import xlwt 
from xlwt import Workbook 

def initializeSIFT():
    orb = cv2.ORB()
    orb = cv2.ORB_create()
    return orb

def getSIFTKeys(orb, img):
    kp, des = orb.detectAndCompute(img, None)
    return kp, des
    
def compareKey(kp1, des1, kp2, des2):
    if len(kp2) == 0 or len(kp1) == 0:
        return 1000
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1,des2)
    matches = sorted(matches, key = lambda x:x.distance)
    firstTen = matches[:10]
    s = 0;
    for i in firstTen:
        s = s + i.distance
    return s/10
    

def trainedImages(path, orb):
    n = 0
    imageNameList = []
    for entry in os.scandir(path):
        if entry.is_file():
            n = n + 1
            imageNameList.append(entry.name)
    kpList = []
    desList = []
    for i in range(n):
#        print("Training", imageNameList[i])
        img = cv2.imread(path+imageNameList[i], 0)
        kp, des = getSIFTKeys(orb, img)
        kpList.append(kp)
        desList.append(des)
#        print(len(kp))
    return kpList, desList, n, imageNameList
        


def test(orb, testpath, kpList, desList, n, tranNameList, T):
    imageNameList = []
    waterMarkedList = []
    k = 0
    for entry in os.scandir(testpath):
        if entry.is_file():
            k = k + 1
            imageNameList.append(entry.name)
            waterMarkedList.append(False)
    for i in range(k):
        file = testpath + imageNameList[i]
        img = cv2.imread(file, 0)
        kp, des = getSIFTKeys(orb, img)
        mi = 10000
        ind = 0
        for j in range(n):
            d = compareKey(kpList[j], desList[j], kp, des)
            if d < mi:
                mi = d
                ind = j
        if mi <= T:
            waterMarkedList[i] = True
    return imageNameList, waterMarkedList, k

def writeXLS(wb, sno, n, nameList, waterList):
    # Workbook is created
    if wb == 0:
        wb = Workbook()
    # add_sheet is used to create sheet. 
    sheet = wb.add_sheet('Sheet '+sno, cell_overwrite_ok=True)
    for i in range(n):
        sheet.write(i, 0, nameList[i])
        sheet.write(i, 1, waterList[i])        
    return wb

def saveXLS(wb, filename):
    wb.save(filename)

def main(trainpath, testpath, T):
    orb = initializeSIFT()
    kpList, desList, n, trainNameList = trainedImages(trainpath, orb)

    T = 40
    imageNameList, waterMarkedList, k = test(orb, testpath, kpList, desList, n, trainNameList, T)
    wb = 0
    jsonScore = {}
    for i in range(k):
        jsonScore[imageNameList[i]] = waterMarkedList[i]
    return jsonScore         
    
if __name__ == "__main__":
#     trainpath = "train/"
#     testpath = "images/amazon_renewed/"
#     T = 40
    trainpath = sys.argv[1]
    testpath = sys.argv[2]
    T = int(sys.argv[3])

    jsonKey = main(trainpath, testpath, T)
    print(jsonKey)
    sys.stdout.flush()
    


# In[ ]:




